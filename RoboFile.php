<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */
class RoboFile extends \Robo\Tasks
{

	function replaceInFile($file, $from, $to) 
	{
		$this->taskReplaceInFile($file)
			->from($from)
			->to($to)
			->run();
	}

	function pregReplaceInFile($file, $from, $to) 
	{	
		$this->taskReplaceInFile($file)
			->regex("[$from([^'\"]*)]")
			->to($to)
			->run();
	}

	function version() 
	{
		$this->taskGitStack()
 			->stopOnFail()
 			->exec("git status -s | cut -c4- | grep -E '\.js$|\.css$' > gitdiff.txt")
 			->printed(true)
 			->run();

 		$destinationFiles = [
 			'public/be/js/scripts/app.js',
 			'public/be/js/scripts/states/menuStates.js',
 			'public/be/js/scripts/states/pageStates.js',
 			'public/fe/scripts/app.js'
 		];

		$files = explode(PHP_EOL, (file_get_contents('gitdiff.txt')));

 		foreach($files as $file) {

 			$filePath = substr($file, 0, 7) == 'public/' ? substr($file, 7) : $file;
			
			$fileVersion = $filePath . '?ver=' . date('ymdHis');

			if($filePath != '') {

				foreach($destinationFiles as $d) {

					$this->pregReplaceInFile($d, $filePath, $fileVersion);

				}

			}

 		}

 		$this->pregReplaceInFile(
			'app/backend/views/layouts/main.volt',
			'be/js/scripts/app.js',
			'be/js/scripts/app.js?ver=' . date('ymdHis')
		);

		$this->pregReplaceInFile(
			'app/frontend/views/layouts/main.volt',
			'fe/scripts/app.js',
			'fe/scripts/app.js?ver=' . date('ymdHis')
		);
	}

}