'use strict';
/**
  * controller for Validation Form example
*/
app.controller('userAddCtrl', ["$scope", "$state", "$timeout", "SweetAlert", "userFctry",  function ($scope, $state, $timeout, SweetAlert, userFctry) {


    $scope.master = $scope.myModel;
    $scope.form = {

        submit: function (form) {
            var firstError = null;
            if (form.$invalid) {

                var field = null, firstError = null;
                for (field in form) {
                    if (field[0] != '$') {
                        if (firstError === null && !form[field].$valid) {
                            firstError = form[field].$name;
                        }

                        if (form[field].$pristine) {
                            form[field].$dirty = true;
                        }
                    }
                }

                angular.element('.ng-invalid[name=' + firstError + ']').focus();
                SweetAlert.swal("The form cannot be submitted because it contains validation errors!", "Errors are marked with a red, dashed border!", "error");
                return;

            } else {
                // SweetAlert.swal("Good job!", "Your form is ready to be submitted!", "success");
                //your code for submit
                this.submitPost($scope.myModel);
            }

        },
        reset: function (form) {
            $scope.myModel = angular.copy($scope.master);
            form.$setPristine(true);

        },

        submitPost : function(data){
            userFctry.$submitPost(data)
        }
    };

}]);
