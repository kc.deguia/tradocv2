'use strict';
/**
* controller for Validation Form example
*/
app.controller('studentListCtrl', ["$scope", "$state", "$timeout", "SweetAlert", "studentFctry", function ($scope, $state, $timeout, SweetAlert, studentFctry) {








    $scope.page = 1;
    $scope.keyword = null;
    $scope.list = 10;
    $scope.pagesize = 5;
    $scope.showsummary = false;
    $scope.showSearch = '';
    $scope.alerts = [];
    $scope.cols = [];
    $scope.searchValue = null;


    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    var loadList = function(){

        // Validate keyword
        if($scope.keyword){
            if($scope.searchValue != $scope.keyword.toLowerCase()){
                $scope.searchValue = $scope.keyword.toLowerCase();
                $scope.page = 1;
            }
        }


        var querydata = {
            'page': $scope.page,
            'keyword': $scope.keyword,
            'count': $scope.list
        }

        studentFctry.$getList(querydata).then(function(data){
            $scope.dataList = data.data;
            $scope.listTotal = data.count;
            $scope.currentPage = data.page;
            $scope.cols = data.cols;
            $scope.start = $scope.page != 1 ? ($scope.page + $scope.list) - 1 : 1;
            $scope.end = ($scope.start + $scope.list) >= $scope.listTotal ? $scope.listTotal : ($scope.start + $scope.list) - 1;
            $scope.end = ($scope.list == 'all' ? $scope.listTotal : $scope.end);

        }, function(error){
            // Error
        })


    };

    $scope.$watch('keyword', function(query){
        if(!query)
        $scope.keyword = null;
        loadList();
    })



    $scope.setPage = function(currentPage){
        if(currentPage){
            $scope.page = currentPage;
            loadList();
        }

    }

    $scope.submitsearch = function(event){
        if (event.which === 13){
            $scope.showsummary = Variable.isEmptyNullUnderfine($scope.keyword) ? false : true;
            $scope.keyword = Variable.isEmptyNullUnderfine($scope.keyword) ? null : $scope.keyword;
            $scope.showSearch = angular.copy($scope.keyword);
            loadList();
        }
    }

    $scope.deleteHospital = function (id){
        swal({
            title: 'Are you sure you want to delete this Facility?',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            preConfirm: function() {
                return new Promise(function(resolve) {
                    swal.enableLoading();
                    var data = {'idHospital':id};
                    hospitalFctry.deleteHospital(data, function(callback){
                        callback ? resolve('success') : resolve('error') ;
                        loadList();
                    });
                });
            },
            allowOutsideClick: false
        }).then(function(result){
            if(result == 'success'){
                swal("Success!", "Facility Successfully deleted!", "success")
            }
            else if(result == 'error'){
                swal("Error!", "Something went wrong please try again  later!", "error")
            }

        })
    };

    $scope.setLimit = function(limit){
        $scope.list = limit;
        $scope.page = 1;
        loadList();
    }



}]);
