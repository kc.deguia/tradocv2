'use strict';

app.factory('userFctry', function ($stateParams, $http, $q, Config) {
    return {
        $submitPost: function (postData) {

            let deferred = $q.defer();

            $http({
                url: Config.ApiURL+ "/members/addmember",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(postData)
            }).success(function (data, status, headers, config){
                deferred.resolve(data.records);
            }).error(function (data, status, headers, config){
                deferred.reject(data.records);
            });
            return deferred.promise;

        }
    }
});
