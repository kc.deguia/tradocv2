<?php
namespace Modules\Backend\Controllers;

use Phalcon\Mvc\View;

class UserController extends ControllerBase
{

    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function addAction($params){
        $this->view->params = $params;
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

}
