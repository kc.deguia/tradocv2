<?php

namespace Modules\Backend\Controllers;  

use Phalcon\Mvc\View;
use Phalcon\Tag;
use Modules\Backend\Models\Users as PI;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
    }
    
    public function logoutAction(){
        session_destroy();
        $service_url = $this->config->application->ApiURL .'/user/logout';

        $curl = curl_init($service_url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $curl_response = curl_exec($curl);

        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additional info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        $this->flash->warning('You have successfully logout.');
        $this->view->pick("index/index");
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function indexAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function forgotpasswordAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function changepasswordAction()
    {
        $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }  
    
}


