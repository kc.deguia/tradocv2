<div class="modal-header">
	<h4 class="modal-title">Add</h4>
</div>
<form name="Form" id="form1" novalidate ng-submit="addData(Form, myModel)" >
	<div class="modal-body">

		<div class="row">
			<div class="col-md-12">
				<div class="form-group" ng-class="{'has-error':Form.schoolName.$dirty && Form.schoolName.$invalid, 'has-success':Form.schoolName.$valid}">
					<label class="control-label">
						NAME OF SCHOOL <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  NAME OF SCHOOL" class="form-control" name="schoolName" ng-model="myModel.schoolName" required />
					<span class="error text-small block" ng-if="Form.schoolName.$dirty && Form.schoolName.$invalid">NAME OF SCHOOL is required</span>
					<span class="success text-small" ng-if="Form.schoolName.$valid">Thank You!</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.location.$dirty && Form.location.$invalid, 'has-success':Form.location.$valid}" >
					<label class="control-label">
						LOCATION <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  LOCATION" class="form-control" name="location" ng-model="myModel.location" required />
					<span class="error text-small block" ng-if="Form.location.$dirty && Form.location.$invalid">LOCATION is required</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.dateOfAttendance.$dirty && Form.dateOfAttendance.$invalid, 'has-success':Form.dateOfAttendance.$valid}">
					<label class="control-label">
						DATE OF ATTENDANCE <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter DATE OF ATTENDANCE" class="form-control" name="dateOfAttendance" ng-model="myModel.dateOfAttendance" required />
					<span class="error text-small block" ng-if="Form.dateOfAttendance.$dirty && Form.dateOfAttendance.$invalid">DATE OF ATTENDANCE is required</span>
					<span class="success text-small" ng-if="Form.dateOfAttendance.$valid">Wonderful!</span>
				</div>
			</div>
		</div>



	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary" >OK</button>
		<button class="btn btn-primary btn-o" ng-click="cancel()">Cancel</button>
	</div>
</form>
