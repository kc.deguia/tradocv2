<div class="modal-header">
	<h5 class="modal-title">Add children</h5>
</div>
<form name="Form" id="form1" novalidate ng-submit="addData(Form, myModel)" >
	<div class="modal-body">

		<div class="row">
			<div class="col-md-12">
				<div class="form-group" ng-class="{'has-error':Form.firstName.$dirty && Form.firstName.$invalid, 'has-success':Form.firstName.$valid}">
					<label class="control-label">
						First Name <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  First Name" class="form-control" name="firstName" ng-model="myModel.firstName" required />
					<span class="error text-small block" ng-if="Form.firstName.$dirty && Form.firstName.$invalid">First Name is required</span>
					<span class="success text-small" ng-if="Form.firstName.$valid">Thank You!</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.middleName.$dirty && Form.middleName.$invalid, 'has-success':Form.middleName.$valid}" >
					<label class="control-label">
						Middle Name <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  Middle Name" class="form-control" name="middleName" ng-model="myModel.middleName" required />
					<span class="error text-small block" ng-if="Form.middleName.$dirty && Form.middleName.$invalid">Middle Name is required</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.lastName.$dirty && Form.lastName.$invalid, 'has-success':Form.lastName.$valid}">
					<label class="control-label">
						Last Name <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter  Last Name" class="form-control" name="lastName" ng-model="myModel.lastName" required />
					<span class="error text-small block" ng-if="Form.lastName.$dirty && Form.lastName.$invalid">Last Name is required</span>
					<span class="success text-small" ng-if="Form.lastName.$valid">Wonderful!</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.birthDate.$dirty && Form.birthDate.$invalid, 'has-success':Form.birthDate.$valid}">
					<label class="control-label">
						Date of Birth <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter Date of Birth" class="form-control" name="birthDate" ng-model="myModel.birthDate" required />
					<span class="error text-small block" ng-if="Form.birthDate.$dirty && Form.birthDate.$invalid">Date of Birth is required</span>
					<span class="success text-small" ng-if="Form.birthDate.$valid">Wonderful!</span>
				</div>

				<div class="form-group" ng-class="{'has-error':Form.address.$dirty && Form.address.$invalid, 'has-success':Form.address.$valid}">
					<label class="control-label">
						Address <span class="symbol required"></span>
					</label>
					<input type="text" placeholder="Enter Address" class="form-control" name="address" ng-model="myModel.address" required />
					<span class="error text-small block" ng-if="Form.address.$dirty && Form.address.$invalid">Address is required</span>
					<span class="success text-small" ng-if="Form.address.$valid">Wonderful!</span>
				</div>
			</div>
		</div>



	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary" >OK</button>
		<button class="btn btn-primary btn-o" ng-click="cancel()">Cancel</button>
	</div>
</form>
