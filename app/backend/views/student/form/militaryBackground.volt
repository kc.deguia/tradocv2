<div class="row">
    <h3 class="over-title margin-bottom-15 text-center"><span class="text-bold"></span></h3>
    <div class="col-md-6">
        <div class="form-group" ng-class="{'has-error':Form.dateEntMilSvc.$dirty && Form.dateEntMilSvc.$invalid, 'has-success':Form.dateEntMilSvc.$valid}">
            <label class="control-label">
                Date Entered Mil Svc (as Trainee or as Cadet)<span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Date Entered Mil Svc (as Trainee or as Cadet)" class="form-control" name="dateEntMilSvc" ng-model="myModel.dateEntMilSvc" required />
            <span class="error text-small block" ng-if="Form.dateEntMilSvc.$dirty && Form.dateEntMilSvc.$invalid">dateEntMilSvcis required</span>
            <span class="success text-small" ng-if="Form.dateEntMilSvc.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.dateEnlisted.$dirty && Form.dateEnlisted.$invalid, 'has-success':Form.dateEnlisted.$valid}">
            <label class="control-label">
                Date of Enlisted <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Date of Enlisted" class="form-control" name="dateEnlisted" ng-model="myModel.dateEnlisted" required />
            <span class="error text-small block" ng-if="Form.dateEnlisted.$dirty && Form.dateEnlisted.$invalid">Date of Enlisted is required</span>
            <span class="success text-small" ng-if="Form.dateEnlisted.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.dateCommision.$dirty && Form.dateCommision.$invalid, 'has-success':Form.dateCommision.$valid}">
            <label class="control-label">
                Date of Commission <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Date of Commission" class="form-control" name="dateCommision" ng-model="myModel.dateCommision" required />
            <span class="error text-small block" ng-if="Form.dateCommision.$dirty && Form.dateCommision.$invalid">Date of Commission is required</span>
            <span class="success text-small" ng-if="Form.dateCommision.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.sourceComission.$dirty && Form.sourceComission.$invalid, 'has-success':Form.sourceComission.$valid}">
            <label class="control-label">
                Source of Comission <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Source of Comission)" class="form-control" name="sourceComission" ng-model="myModel.sourceComission" required />
            <span class="error text-small block" ng-if="Form.sourceComission.$dirty && Form.sourceComission.$invalid">Source of Comission is required</span>
            <span class="success text-small" ng-if="Form.sourceComission.$valid">Thank You!</span>
        </div>


    </div>

    <div class="col-md-6">
        <div class="form-group" ng-class="{'has-error':Form.dateCad.$dirty && Form.dateCad.$invalid, 'has-success':Form.dateCad.$valid}">
            <label class="control-label">
                Date of CAD <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Date of CAD" class="form-control" name="dateCad" ng-model="myModel.dateCad" required />
            <span class="error text-small block" ng-if="Form.dateCad.$dirty && Form.dateCad.$invalid">Date of CAD is required</span>
            <span class="success text-small" ng-if="Form.dateCad.$valid">Thank You!</span>
        </div>
        <div class="form-group" ng-class="{'has-error':Form.sepMilSvc.$dirty && Form.sepMilSvc.$invalid, 'has-success':Form.sepMilSvc.$valid}">
            <label class="control-label">
                Have you ever been separated from the Military Service?  <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Have you ever been separated from the Military Service? " class="form-control" name="sepMilSvc" ng-model="myModel.sepMilSvc" required />
            <span class="error text-small block" ng-if="Form.sepMilSvc.$dirty && Form.sepMilSvc.$invalid">Have you ever been separated from the Military Service?  is required</span>
            <span class="success text-small" ng-if="Form.sepMilSvc.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.sateNature.$dirty && Form.sateNature.$invalid, 'has-success':Form.sateNature.$valid}">
            <label class="control-label">
                If yes, state nature and circumstances <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter If yes, state nature and circumstances " class="form-control" name="sateNature" ng-model="myModel.sateNature" required />
            <span class="error text-small block" ng-if="Form.sateNature.$dirty && Form.sateNature.$invalid">If yes, state nature and circumstances is required</span>
            <span class="success text-small" ng-if="Form.sateNature.$valid">Thank You!</span>
        </div>
    </div>
</div>



<fieldset>
    <legend><h5><b>IMPORTANT UNIT ASSIGNMENT</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'milBackground.html', 'importUnitASsignment', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>DESIGNATION</th>
                <th>INCLUSIVE DATE </th>
                <th>UNITE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.importUnitASsignment">
                <td class=""><span ng-bind-html="list.designation"></span></td>
                <td class=""><span ng-bind-html="list.inclusiveDate"></span></td>
                <td class=""><span ng-bind-html="list.unit"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'milBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.importUnitASsignment)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>


<fieldset>
    <legend><h5><b>MILITARY SCHOOLING ATTENDED</b></h5>	</legend>
    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'milBackground.html', 'milSchoolAttended', myModel )">
        <i class="glyphicon glyphicon-plus"></i> ADD
    </button>
    <h5 class="over-title margin-bottom-15"> <span class="text-bold"></span></h5>
    <table class="table table-hover" id="sample-table-1">
        <thead>
            <tr>
                <th>DESIGNATION</th>
                <th>INCLUSIVE DATE </th>
                <th>UNITE</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat="list in myModel.milSchoolAttended">
                <td class=""><span ng-bind-html="list.designation"></span></td>
                <td class=""><span ng-bind-html="list.inclusiveDate"></span></td>
                <td class=""><span ng-bind-html="list.unit"></span></td>
                <td class="center">
                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'milBackground.html', list)"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.milSchoolAttended)"><i class="fa fa-times fa fa-white"></i></a>
                    </div>
                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                        <div class="btn-group" uib-dropdown>
                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                <li>
                                    <a href="#">
                                        Edit
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Remove
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>
