<div class="row">

    <div class="col-md-6">
        <div class="form-group" ng-class="{'has-error':Form.maritalStatus.$dirty && Form.maritalStatus.$invalid, 'has-success':Form.maritalStatus.$valid}">
            <label class="control-label">
                Marital Status<span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Marital Status" class="form-control" name="maritalStatus" ng-model="myModel.maritalStatus" required />
            <span class="error text-small block" ng-if="Form.maritalStatus.$dirty && Form.maritalStatus.$invalid">Marital Status required</span>
            <span class="success text-small" ng-if="Form.maritalStatus.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.spouseName.$dirty && Form.spouseName.$invalid, 'has-success':Form.spouseName.$valid}">
            <label class="control-label">
                Name of Spouse <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter spouseName" class="form-control" name="spouseName" ng-model="myModel.spouseName" required />
            <span class="error text-small block" ng-if="Form.spouseName.$dirty && Form.spouseName.$invalid">	Name of Spouse is required</span>
            <span class="success text-small" ng-if="Form.spouseName.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.spouseBirthdate.$dirty && Form.spouseBirthdate.$invalid, 'has-success':Form.spouseBirthdate.$valid}">
            <label class="control-label">
                Spouse Date of Birth: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Spouse Date of Birth:" class="form-control" name="spouseBirthdate" ng-model="myModel.spouseBirthdate" required />
            <span class="error text-small block" ng-if="Form.spouseBirthdate.$dirty && Form.spouseBirthdate.$invalid">Spouse Date of Birth: is required</span>
            <span class="success text-small" ng-if="Form.spouseBirthdate.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.marriageDate.$dirty && Form.marriageDate.$invalid, 'has-success':Form.marriageDate.$valid}">
            <label class="control-label">
                Date of Marriage: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Date of Marriage" class="form-control" name="marriageDate" ng-model="myModel.marriageDate" required />
            <span class="error text-small block" ng-if="Form.marriageDate.$dirty && Form.marriageDate.$invalid">Date of Marriage is required</span>
            <span class="success text-small" ng-if="Form.marriageDate.$valid">Thank You!</span>
        </div>

    </div>

    <div class="col-md-6">

        <div class="form-group" ng-class="{'has-error':Form.spouseOccupation.$dirty && Form.spouseOccupation.$invalid, 'has-success':Form.spouseOccupation.$valid}">
            <label class="control-label">
                Occupation and Place of Employment: <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Occupation and Place of Employment" class="form-control" name="spouseOccupation" ng-model="myModel.spouseOccupation" required />
            <span class="error text-small block" ng-if="Form.spouseOccupation.$dirty && Form.spouseOccupation.$invalid">Occupation and Place of Employment is required</span>
            <span class="success text-small" ng-if="Form.spouseOccupation.$valid">Thank You!</span>
        </div>


        <div class="form-group" ng-class="{'has-error':Form.spouseCitizenship.$dirty && Form.spouseCitizenship.$invalid, 'has-success':Form.spouseCitizenship.$valid}">
            <label class="control-label">
                Citizenship <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter Citizenship" class="form-control" name="spouseCitizenship" ng-model="myModel.spouseCitizenship" required />
            <span class="error text-small block" ng-if="Form.spouseCitizenship.$dirty && Form.spouseCitizenship.$invalid">Citizenship is required</span>
            <span class="success text-small" ng-if="Form.spouseCitizenship.$valid">Thank You!</span>
        </div>

        <div class="form-group" ng-class="{'has-error':Form.ifNaturalized.$dirty && Form.ifNaturalized.$invalid, 'has-success':Form.ifNaturalized.$valid}">
            <label class="control-label">
                If naturalized, give date and place where naturalized <span class="symbol required"></span>
            </label>
            <input type="text" placeholder="Enter If naturalized, give date and place where naturalized " class="form-control" name="ifNaturalized" ng-model="myModel.ifNaturalized" required />
            <span class="error text-small block" ng-if="Form.ifNaturalized.$dirty && Form.ifNaturalized.$invalid">If naturalized, give date and place where naturalized is required</span>
            <span class="success text-small" ng-if="Form.ifNaturalized.$valid">Thank You!</span>
        </div>

    </div>

    <div class="col-md-12">
        <div class="container-fluid container-fullw bg-white">
        	<div class="row">
        		<div class="col-md-12">
                    <button type="button" class="btn btn-primary btn-xs pull-right" ng-click="form.modal('', 'children.html', 'children', myModel)">
						<i class="glyphicon glyphicon-plus"></i> ADD
					</button>
        			<h5 class="over-title margin-bottom-15">CHILDREN <span class="text-bold"></span></h5>
        			<table class="table table-hover" id="sample-table-1">
        				<thead>
        					<tr>
        						<th>Name</th>
        						<th >Date of Birth</th>
        						<th>Address</th>
        						<th></th>
        					</tr>
        				</thead>
        				<tbody>
        					<tr ng-repeat="list in myModel.children">
        						<td class=""><span ng-bind-html="list.firstName +' '+ list.middleName +' '+ list.lastName"></span></td>
        						<td class=""><span ng-bind-html="list.birthDate"></span></td>
                                <td class=""><span ng-bind-html="list.address"></span></td>
                                <td class="center">
                                    <div class="visible-md visible-lg hidden-sm hidden-xs">
                                        <a href="#" class="btn btn-transparent btn-xs" tooltip-placement="top" uib-tooltip="Edit" ng-click="form.modalEdit('', 'children.html', list)"><i class="fa fa-pencil"></i></a>
                                        <a href="#" class="btn btn-transparent btn-xs tooltips" tooltip-placement="top" uib-tooltip="Remove" ng-click="form.removeList($index, myModel.children)"><i class="fa fa-times fa fa-white"></i></a>
                                    </div>
                                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                                        <div class="btn-group" uib-dropdown>
                                            <button type="button" class="btn btn-primary btn-o btn-sm dropdown-toggle" uib-dropdown-toggle>
                                                <i class="fa fa-cog"></i>&nbsp;<span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right dropdown-light" role="menu">
                                                <li>
                                                    <a href="#">
                                                        Edit
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        Remove
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
        					</tr>
        				</tbody>
        			</table>
        		</div>
        	</div>
        </div>
    </div>




</div>
